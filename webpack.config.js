const path = require('path');

module.exports = {
    mode: 'production',
    entry: './cfworker/index.js',
    target: 'webworker',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js',
    },
    module: {
        rules: [
            {
                test: /\.html$/i,
                loader: 'html-loader',
            },
        ],
    },
};
