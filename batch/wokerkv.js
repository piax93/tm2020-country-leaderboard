const axios = require('axios').default;

const CF_API_BASE_URL = 'https://api.cloudflare.com/client/v4/';

class CFWorkerKV {
    /**
     * @param {string} token       Cloudflare API token
     * @param {string} accountId   Cloudflare account ID
     * @param {string} namespaceId KV namespace ID
     */
    constructor(token, accountId, namespaceId) {
        this.accountId = accountId;
        this.namespaceId = namespaceId;
        this.headers = { Authorization: `Bearer ${token}` };
    }

    composeKeyURI(key) {
        const enckey = encodeURIComponent(key);
        const path = `accounts/${this.accountId}/storage/kv/namespaces/${this.namespaceId}/values/${enckey}`;
        return `${CF_API_BASE_URL}${path}`;
    }

    /**
     * Add entry to worker KV database
     *
     * @param {string} key   Entry key
     * @param {string} value Entry value
     * @param {number} ttl   Entry time to live in seconds
     * @returns {object}     HTTP response body
     */
    async putKVEntry(key, value, ttl = null) {
        const params = ttl ? { expiration_ttl: ttl } : {};
        const headers = { ...this.headers, 'Content-Type': 'text/plain' };
        const response = await axios({
            method: 'PUT',
            url: this.composeKeyURI(key),
            params,
            headers,
            data: value,
        });
        return response.data;
    }

    /**
     * Get entry from KV database
     *
     * @param {string} key Entry key
     * @returns {string}   Entry value
     */
    async getKVEntry(key) {
        const response = await axios({
            method: 'GET',
            url: this.composeKeyURI(key),
            headers: this.headers,
        });
        return response.data;
    }

    /**
     * List all entry keys in database
     *
     * @returns {array<string>} List of keys
     */
    async listEntries() {
        const path = `accounts/${this.accountId}/storage/kv/namespaces/${this.namespaceId}/keys`;
        const response = await axios({
            method: 'GET',
            url: `${CF_API_BASE_URL}${path}`,
            headers: this.headers,
        });
        return response.data.result.map(e => e.name);
    }
}

exports.CFWorkerKV = CFWorkerKV;
