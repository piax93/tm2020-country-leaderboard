const chunkArray = require('chunk');
const flatten = require('array-flatten');
const randomInteger = require('random-int');

/**
 * Apply function on a list of elements in chunks
 *
 * @param {array} parameters      List of parameters to chunk
 * @param {number} chunkSize      Size of the chunks
 * @param {Function} callable     Async function to call on parameters
 * @param {Function} resultMapper Function for mapping results before flattening
 * @returns                       Call results
 */
async function chunkMethodCall(parameters, chunkSize, callable, resultMapper) {
    const chunks = chunkArray(parameters, chunkSize).map(callable);
    const responses = await Promise.all(chunks);
    return flatten(responses.map(resultMapper));
}

/**
 * Call function accepting an offset parameter and return the concatenated results
 *
 * @param {number} maxEntries     Max number of entries to return
 * @param {function} callable     Async function accepting offset parameter
 * @param {function} resultMapper Function mapping result
 * @param {boolean} doTinySleeps  If set, wait a bit between function calls
 * @returns {array}               Joined result from the function calls
 */
async function joinOffsetMethodCall(maxEntries, callable, resultMapper, doTinySleeps = true) {
    let offset = 0;
    let result = [];
    while (offset < maxEntries) {
        const entries = resultMapper(await callable(offset));
        if (!entries) {
            break;
        }
        result.push(...entries);
        offset = result.length;
        if (doTinySleeps) {
            await tinyRandomSleep();
        }
    }
    return result;
}

/**
 * Sleep for some time
 *
 * @param {number} millis Milliseconds
 */
async function sleep(millis) {
    return new Promise(r => setTimeout(r, millis));
}

/**
 * Sleep for a small random time ([50, 100] milliseconds)
 */
async function tinyRandomSleep() {
    return sleep(randomInteger(50, 100));
}

exports.sleep = sleep;
exports.chunkMethodCall = chunkMethodCall;
exports.tinyRandomSleep = tinyRandomSleep;
exports.joinOffsetMethodCall = joinOffsetMethodCall;
