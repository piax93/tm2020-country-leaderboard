const { computeCampaignScore } = require('./scoring');
const { getLeaderboardsWithOffset, getGroupLeaderboardWithOffset, getUsernames, MAX_PROFILES_BATCH } = require('./api');
const { chunkMethodCall, sleep, joinOffsetMethodCall } = require('./util');
const tmapis = require('trackmania-api-node');
const fs = require('fs');
const path = require('path');

const MAX_POSITIONS_CRAWL = 3000;
const MAX_POSITIONS_CRAWL_CAMPAIGN = 10000;
const DEFAULT_NOT_FOUND_POSITION = 10000;
const TOP_REGION_POSITIONS = 5;

function padPositionArray(arr, len) {
    return arr.concat(Array(len).fill(DEFAULT_NOT_FOUND_POSITION)).slice(0, len);
}

class TMClient {
    /**
     * Login to Trackmania APIs
     *
     * @param {string} username Uplay username
     * @param {string} password Uplay password
     * @param {string} lvl1refresh Level 1 auth refresh token
     */
    async login(username, password, lvl1refresh = null, oauthToken = null) {
        let lvl1auth;
        if (lvl1refresh) {
            lvl1auth = await tmapis.refreshTokens(lvl1refresh);
        } else {
            const creds = Buffer.from(`${username}:${password}`).toString('base64');
            const { ticket } = await tmapis.loginUbi(creds);
            lvl1auth = await tmapis.loginTrackmaniaUbi(ticket);
        }
        this.lvl1token = lvl1auth.accessToken;
        this.lvl1refresh = lvl1auth.refreshToken;
        const lvl2auth = await tmapis.loginTrackmaniaNadeo(this.lvl1token, 'NadeoLiveServices');
        this.lvl2token = lvl2auth.accessToken;
        this.oauthToken = oauthToken;
    }

    /**
     * Get info about official campaign (latest by default)
     *
     * @param {string} season Fetch specific season (e.g. "fall2022")
     *
     * @returns {object} campaign ID and leaderboard group ID
     */
    async getCampaignInfo(season = null) {
        const allSeasons = await tmapis.getSeasons(this.lvl2token, 0, 1000);
        const campaignInfo = season
            ? allSeasons.campaignList.find(c => c.name.toLowerCase().replace(' ', '') == season)
            : allSeasons.campaignList[0];
        return {
            campaignId: campaignInfo.seasonUid,
            leaderGroup: campaignInfo.leaderboardGroupUid,
        };
    }

    /**
     * List all map IDs in a given campaign
     *
     * @param {string} leaderGroup Campaign leaderboard group ID
     * @returns {array<string>}    List of map IDs
     */
    async getCampaignMaps(leaderGroup) {
        const myrecords = await tmapis.getMyGroupRecords(this.lvl2token, leaderGroup);
        return Object.keys(myrecords);
    }

    /**
     * Get leaderbaord for a map in a campaign
     *
     * @param {string} leaderGroup Campaign leaderboard group ID
     * @param {string} mapId       Map ID
     * @param {number} maxEntries  Number of positions to return
     * @returns {array<object>}    List of player positions
     */
    fetchMapLeaderBoard(leaderGroup, mapId, maxEntries) {
        return joinOffsetMethodCall(
            maxEntries,
            offset => getLeaderboardsWithOffset(this.lvl2token, leaderGroup, mapId, offset),
            res => res.tops[0].top,
        );
    }

    /**
     * Get leaderbaord for a campaign
     *
     * @param {string} leaderGroup Campaign leaderboard group ID
     * @param {number} maxEntries  Number of positions to return
     * @returns {array<object>}    List of player positions
     */
    fetchCampaignLeaderboard(leaderGroup, maxEntries) {
        return joinOffsetMethodCall(
            maxEntries,
            offset => getGroupLeaderboardWithOffset(this.lvl2token, leaderGroup, offset),
            res => res.tops[0].top,
        );
    }

    /**
     * Enumerate zone IDs for a country
     *
     * @param {string} country  Common name for country
     * @returns {array<string>} List of zone IDs
     */
    async enumerateCountryZones(country) {
        let result = new Set();
        const allZones = await tmapis.getZones(this.lvl1token);
        const countryZone = allZones.find(z => z.name == country).zoneId;
        let bfs = [countryZone];
        result.add(countryZone);
        while (bfs.length > 0) {
            const curr = bfs.shift();
            const childZones = allZones.filter(z => z.parentId == curr && !result.has(z.zoneId)).map(z => z.zoneId);
            bfs.push(...childZones);
            childZones.forEach(z => result.add(z));
        }
        return result;
    }

    /**
     * Get trophy information for players
     *
     * @param {array<string>} profileIDs List of profile IDs
     * @param {string} zoneName          Geo-zone of interest
     * @returns {array<object>}          List trophy-score information
     */
    async getTrophyPositions(profileIDs, zoneName) {
        const userData = await chunkMethodCall(
            profileIDs,
            MAX_PROFILES_BATCH,
            c => tmapis.getPlayerRankings(this.lvl2token, c),
            r => r.rankings,
        );
        return userData
            .map(d => {
                let zone = d.zones.find(z => z.zoneName == zoneName);
                return zone
                    ? {
                          id: d.accountId,
                          pos: zone.ranking.position,
                          score: d.countPoint,
                          world: d.zones[0].ranking.position,
                          region: d.zones[d.zones.length - 1].zoneName,
                      }
                    : false;
            })
            .filter(d => d);
    }

    /**
     * Compute campaign leaderboard for a country
     *
     * @param {string} country      Common name for country
     * @param {number} size         Number of entries to return
     * @param {boolean} notrophy    Do not calculate trophy leaderboard (default=false)
     * @param {boolean} legacyFetch We used to have to crawl all 25 maps, now Nadeo let's use query the campaign leaderboard directly!!
     *                              Set this to true to go back to old way of doing things (default=false)
     * @param {string} season       Fetch a specific season rather than latest (e.g. "fall2022")
     * @returns {object}            `campaign` and `trophy` leaderboards
     */
    async computeLeaderboardForCountry(country, size, notrophy = false, legacyFetch = false, season = null) {
        let playerPositions = {};
        let positionMapper = null;
        const { leaderGroup } = await this.getCampaignInfo(season);
        const zones = await this.enumerateCountryZones(country);
        console.log('Fetched information about current campaign and player zones');
        if (legacyFetch) {
            let allLeaderboards = [];
            const maps = await this.getCampaignMaps(leaderGroup);
            for (const m of maps) {
                // we do this slowly on purpose
                allLeaderboards.push(await this.fetchMapLeaderBoard(leaderGroup, m, MAX_POSITIONS_CRAWL));
                console.log('Completed scraping map %s', m);
                await sleep(333);
            }
            for (const leaderboard of allLeaderboards) {
                for (const player of leaderboard) {
                    if (zones.has(player.zoneId)) {
                        if (player.accountId in playerPositions) {
                            playerPositions[player.accountId].push(player.position);
                        } else {
                            playerPositions[player.accountId] = [player.position];
                        }
                    }
                }
            }
            positionMapper = e => [e[0], [computeCampaignScore(padPositionArray(e[1], maps.length))]];
        } else {
            playerPositions = Object.fromEntries(
                (await this.fetchCampaignLeaderboard(leaderGroup, MAX_POSITIONS_CRAWL_CAMPAIGN))
                    .map((player, i) => [i + 1, player])
                    .filter(player => zones.has(player[1].zoneId))
                    .map(player => [player[1].accountId, [parseInt(player[1].sp), player[0]]]),
            );
            positionMapper = e => e;
        }
        const finalBoard = Object.entries(playerPositions)
            .map(positionMapper)
            .sort((a, b) => b[1][0] - a[1][0])
            .slice(0, size);
        let topPlayers = [];
        let idsToUsernames = {};
        const topPlayerPath = path.resolve(__dirname, 'top_players.json');
        if (fs.existsSync(topPlayerPath)) {
            idsToUsernames = JSON.parse(fs.readFileSync(topPlayerPath));
            topPlayers = Object.keys(idsToUsernames);
        }
        const trophyPlayers = Array.from(new Set([...Object.keys(playerPositions), ...topPlayers]));
        const allTrophies = notrophy
            ? []
            : (await this.getTrophyPositions(trophyPlayers, country)).sort((a, b) => b.score - a.score);
        const regionTops = Object.entries(
            allTrophies.reduce((result, player) => {
                (result[player.region] = result[player.region] || []).push(player);
                return result;
            }, {}),
        )
            .map(e => {
                return {
                    id: '',
                    name: e[0],
                    score: e[1].slice(0, TOP_REGION_POSITIONS).reduce((res, i) => res + i.score, 0),
                };
            })
            .sort((a, b) => b.score - a.score);
        const finalTrophies = allTrophies.slice(0, size);
        const playerIds = Array.from(new Set([...finalBoard.map(e => e[0]), ...finalTrophies.map(e => e.id)])).filter(
            i => !idsToUsernames[i],
        );
        console.log('Found %d players from %s in total', playerIds.length, country);
        idsToUsernames = { ...idsToUsernames, ...(await getUsernames(this.oauthToken, playerIds)) };
        return {
            campaign: finalBoard.map(e => {
                return {
                    id: e[0],
                    name: idsToUsernames[e[0]],
                    score: e[1][0],
                    world: e[1][1],
                };
            }),
            trophy: finalTrophies.map(e => {
                e.name = idsToUsernames[e.id];
                delete e.region;
                return e;
            }),
            regions: regionTops,
        };
    }
}

exports.TMClient = TMClient;
