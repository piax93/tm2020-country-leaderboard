/* eslint no-unused-vars: ["error", { "varsIgnorePattern": "^calculatePoint$" }] */

/**
 * Compute campaign score given placement
 * The one used for https://geri43.github.io/TrackmaniaSeasonPointCalculator/
 *
 * @param {number} p Placement
 * @returns {number} Score
 */
function calculatePoint(p) {
    const tier = Math.ceil(Math.log10(p));
    if (tier < 2) {
        return 40000 / p;
    } else if (tier < 7) {
        const basePoints = 4000 / Math.pow(2, tier - 1);
        const rankMultiplier = Math.pow(10, tier - 1) / p + 0.9;
        return basePoints * rankMultiplier;
    }
    return 0;
}

/**
 * Compute campaign score given placement
 * The one suggested by Kem; same result, but looks cleaner
 *
 * @param {number} p Placement
 * @returns {number} Score
 */
function calculatePointKem(p) {
    const tier = Math.ceil(Math.log10(p));
    if (tier < 2) {
        return 40000 / p;
    }
    const compA = 4000 * Math.pow(5, tier - 1);
    const compB = 3600 / Math.pow(2, tier - 1);
    return compA / p + compB;
}

/**
 * Compute campaign score given a list of placements
 *
 * @param {array<number>} positions List of placements
 * @returns {number}                Score
 */
function computeCampaignScore(positions) {
    return positions.map(v => Math.round(calculatePointKem(v))).reduce((curr, v) => curr + v, 0);
}

exports.computeCampaignScore = computeCampaignScore;
