// Extension of the API library with the newer endpoints
const axios = require('axios').default;
const qs = require('qs');
const { setHeaders, urls } = require('trackmania-api-node');
const { chunkMethodCall } = require('./util');

const LEADERBOARD_PAGE_LEN = 100;
const MAX_PROFILES_BATCH = 40;
const LEADERBOARD_MAX_OFFSET = 10000;
const NADEO_API_BASE = 'https://api.trackmania.com/api';

/**
 * Get a portion of a leaderboard given a map and a group
 *
 * @param {string} token    LVL2 access token
 * @param {string} groupID  ID of the leaderboard group
 * @param {string} mapID    ID of the map
 * @param {number} offset   Offset to fetch
 * @returns {array<object>} List of player positions (100 elements)
 */
async function getLeaderboardsWithOffset(token, groupID, mapID, offset) {
    const headers = setHeaders(token, 'nadeo');
    const response = await axios({
        method: 'GET',
        url: `${urls.liveServices}/api/token/leaderboard/group/${groupID}/map/${mapID}/top`,
        params: {
            length: LEADERBOARD_PAGE_LEN,
            onlyWorld: true,
            offset: Math.min(offset, LEADERBOARD_MAX_OFFSET),
        },
        headers,
    });
    return response.data;
}

/**
 * Get a portion of a campaign leaderboard
 *
 * @param {string} token   LVL2 access token
 * @param {string} groupID ID of the leaderboard group
 * @param {number} offset  Offset to fetch
 * @returns {array}        List of player positions
 */
async function getGroupLeaderboardWithOffset(token, groupID, offset) {
    const headers = setHeaders(token, 'nadeo');
    const response = await axios({
        method: 'GET',
        url: `${urls.liveServices}/api/token/leaderboard/group/${groupID}/top`,
        params: {
            length: LEADERBOARD_PAGE_LEN,
            onlyWorld: true,
            offset: Math.min(offset, LEADERBOARD_MAX_OFFSET),
        },
        headers,
    });
    return response.data;
}

/**
 * Map user IDs to usernames
 *
 * @param {string} token             LVL1 access token
 * @param {array<string>} profileIDs List of user IDs
 * @returns {object}                 Map from user IDs to usernames
 */
async function getUsernames(token, profileIDs) {
    const headers = { Authorization: `Bearer ${token}` };
    const userData = await chunkMethodCall(
        profileIDs,
        MAX_PROFILES_BATCH,
        c => {
            return axios({
                method: 'GET',
                url: `${NADEO_API_BASE}/display-names`,
                params: { accountId: c },
                paramsSerializer: p => qs.stringify(p, { encodeValuesOnly: true }).replace(/\[\d+\]/g, '[]'),
                headers,
            });
        },
        r => r.data,
    );
    return userData.reduce((p, c) => {
        return { ...p, ...c };
    }, {});
}

/**
 * Perform Oauth athentication with Nadeo endpoint
 *
 * @param {string} clientId     Oauth client ID
 * @param {string} clientSecret Oauth client secret
 */
async function fetchOauthToken(clientId, clientSecret) {
    const response = await axios({
        method: 'POST',
        url: `${NADEO_API_BASE}/access_token`,
        data: {
            grant_type: 'client_credentials',
            client_id: clientId,
            client_secret: clientSecret,
        },
    });
    return response.data.access_token;
}

exports.getLeaderboardsWithOffset = getLeaderboardsWithOffset;
exports.getGroupLeaderboardWithOffset = getGroupLeaderboardWithOffset;
exports.getUsernames = getUsernames;
exports.fetchOauthToken = fetchOauthToken;
exports.MAX_PROFILES_BATCH = MAX_PROFILES_BATCH;
