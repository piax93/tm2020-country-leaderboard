const { TMClient } = require('./trackmania');
const { CFWorkerKV } = require('./wokerkv');
const { prompt } = require('enquirer');
const yargs = require('yargs/yargs');
const { hideBin } = require('yargs/helpers');
const { fetchOauthToken } = require('./api');

const DEFAULT_COUNTRY = 'Italy';
const DEFAULT_LEADERBOARD_SIZE = 100;
const DATA_TTL = 60 * 60 * 24 * 30; // 30 days
const KV_TOKEN_KEY = 'NANDO_REFRESH';
const KV_OAUTH_KEY = 'NANDO_OAUTH';

function initCFWorkerKV() {
    return process.env.CF_KV_API_KEY
        ? new CFWorkerKV(process.env.CF_KV_API_KEY, process.env.CF_ACCOUNT_ID, process.env.CF_KV_ID)
        : null;
}

async function scrape_leaderboard(tokeninkv, notrophy, snapseason) {
    let tm = new TMClient();
    let kv = initCFWorkerKV();
    let now = Math.round(new Date().getTime() / 1000);
    const country = process.env.TM2020_COUNTRY || DEFAULT_COUNTRY;
    const lbsize = process.env.TM2020_LB_SIZE || DEFAULT_LEADERBOARD_SIZE;
    try {
        let maybeOauthToken = null;
        let maybeToken = process.env.TM2020_RTOKEN;
        if (tokeninkv && kv) {
            maybeToken = await kv.getKVEntry(KV_TOKEN_KEY);
            let maybeOauthCreds = await kv.getKVEntry(KV_OAUTH_KEY);
            if (maybeOauthCreds) {
                maybeOauthToken = await fetchOauthToken(maybeOauthCreds.client_id, maybeOauthCreds.client_secret);
            }
        }
        await tm.login(process.env.TM2020_USERNAME, process.env.TM2020_PASSWORD, maybeToken, maybeOauthToken);
        console.log('Computing first %d positions for %s', lbsize, country);
        const leaderboard = await tm.computeLeaderboardForCountry(country, lbsize, notrophy, false, snapseason);
        console.log('Leaderboard scraped:');
        console.log(leaderboard);
        if (kv) {
            leaderboard.updated = now;
            console.log('Storing data into worker KV');
            const leaderStr = JSON.stringify(leaderboard);
            if (snapseason) {
                await kv.putKVEntry(snapseason, leaderStr);
            } else {
                now = now.toString();
                await kv.putKVEntry(now, leaderStr, DATA_TTL);
                await kv.putKVEntry('latest', now);
            }
            if (tokeninkv) {
                await kv.putKVEntry(KV_TOKEN_KEY, tm.lvl1refresh);
            }
        }
        console.log('All done!');
    } catch (e) {
        console.log(e.toJSON ? e.toJSON() : e);
        if (kv && tokeninkv && tm.lvl1refresh) {
            await kv.putKVEntry(KV_TOKEN_KEY, tm.lvl1refresh);
        }
        process.exit(1);
    }
}

async function generate_token(tokeninkv) {
    const resp = await prompt([
        {
            type: 'input',
            name: 'username',
            message: 'Uplay login email:',
        },
        {
            type: 'password',
            name: 'password',
            message: 'Uplay password:',
        },
    ]);
    let tm = new TMClient();
    await tm.login(resp.username, resp.password);
    if (tokeninkv) {
        let kv = initCFWorkerKV();
        await kv.putKVEntry(KV_TOKEN_KEY, tm.lvl1refresh);
        console.log('Refresh token stored in KV');
    } else {
        console.log('export TM2020_RTOKEN="%s"', tm.lvl1refresh);
    }
}

async function main() {
    const validCommands = {
        run: 'Run leaderboard scraping',
        token: 'Generate authentication refresh token',
    };
    let parser = yargs(hideBin(process.argv));
    Object.entries(validCommands).forEach(e => parser.command(...e));
    const argv = parser
        .usage('Usage: $0 <command>')
        .option('tokeninkv', { description: 'Store/Load auth token from worker KV store' })
        .boolean('tokeninkv')
        .option('notrophy', { description: 'Do not calculate trophy leaderboard' })
        .boolean('notrophy')
        .option('snapshot', { description: 'Create snapshot for specific campaign' })
        .help('h')
        .alias('h', 'help')
        .demandCommand(1).argv;
    await (argv._[argv._.length - 1] == 'token' ? generate_token : scrape_leaderboard)(
        argv.tokeninkv,
        argv.notrophy,
        argv.snapshot,
    );
}

main();
