.PHONY: publish clean scrape cfenv

.account_id:
	wrangler whoami | grep -Eo '[0-9a-f]{32}' > $@

.kvstore_id:
	make wrangler.toml NOKV=true
	wrangler kv:namespace list | grep tm2020leader | grep -Eo '[0-9a-f]{32}' > $@

ifdef NOKV
wrangler.toml: wrangler.toml.tpl .account_id
	cat wrangler.toml.tpl | sed 's/MY_ACCOUNT_ID/$(shell cat .account_id)/' > wrangler.toml
else
wrangler.toml: wrangler.toml.tpl .account_id .kvstore_id
	cat wrangler.toml.tpl \
		| sed 's/MY_ACCOUNT_ID/$(shell cat .account_id)/' \
		| sed 's/KVSTORE_ID/$(shell cat .kvstore_id)/g' \
		> wrangler.toml
endif

publish: wrangler.toml
	wrangler publish

scrape:
	npm run scraper

snapscrape:
	npm run scraper -- --snapshot $(SEASON)

clean:
	rm -rf node_modules wrangler.toml dist .account_id .kvstore_id

cfenv: .account_id .kvstore_id
	@echo 'export CF_ACCOUNT_ID=$(shell cat .account_id)'
	@echo 'export CF_KV_ID=$(shell cat .kvstore_id)'

install-hooks:
	python3 -m pre_commit install -f --install-hooks
