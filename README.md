# TM2020 Country Leaderboard

Scrapes player positions from current campaign maps and creates a leaderboards for a geo-zone.

## Usage

```bash
$ node batch/main.js -h
Usage: main.js <command>

Commands:
  main.js run    Run leaderboard scraping
  main.js token  Generate authentication refresh token

Options:
      --version    Show version number                                 [boolean]
      --tokeninkv  Store/Load auth token from worker KV store          [boolean]
      --notrophy   Do not calculate trophy leaderboard                 [boolean]
  -h, --help       Show help                                           [boolean]
```

### Input variables

These are environment variables used as input for the batch.

```bash
# Trackmania API interaction
TM2020_USERNAME  # Uplay username for API authentication
TM2020_PASSWORD  # Uplay password for API authentication
TM2020_RTOKEN    # Nadeo refresh token (alternative to Uplay credentials)
TM2020_LB_SIZE   # Number of entries to return on the leaderboards (default: 100)
TM2020_COUNTRY   # Geo-zone to consider for the leaderboards (default: Italy)

# Cloudflare worker interaction
CF_ACCOUNT_ID    # Cloudflare account ID
CF_KV_ID         # Cloudflare KV storage namespace ID
CF_KV_API_KEY    # Cloudflare API key with write access to KV namespace
```

## Repo structure

```bash
/
├─ batch/              # Code for scraping the campaign leaderboards
├─ cfworker/           # Cloudflare worker for serving a frontend
├─ wrangler.toml.tpl   # Cloudflare worker configuration (but just a template
|                      # cause I don't want to commit my account details)
├─ ...                 # The usual other bunch of files
```
