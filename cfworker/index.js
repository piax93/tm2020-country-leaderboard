/* eslint-env worker */
/* eslint no-unused-vars: ["error", { "argsIgnorePattern": "_" }] */
/* global LEADERBOARD_KV */

import html from './index.html';

addEventListener('fetch', event => {
    event.respondWith(handleRequest(event.request));
});

async function handleRequest(request) {
    const { searchParams } = new URL(request.url);
    const versionParam = searchParams.get('version');
    const latest =
        versionParam && versionParam.match(/^(\d+|(winter|spring|summer|fall)\d{4})$/)
            ? versionParam
            : await LEADERBOARD_KV.get('latest');
    const leaderboard_data = await LEADERBOARD_KV.get(latest);
    return new Response(html.replace('LEADERBOARD_DATA_HERE', leaderboard_data), {
        headers: { 'content-type': 'text/html' },
    });
}
