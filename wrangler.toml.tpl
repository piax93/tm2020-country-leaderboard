name = "tm2020ita"
type = "javascript"
account_id = "MY_ACCOUNT_ID"
workers_dev = true
zone_id = ""
route = ""
compatibility_date = "2021-12-22"
kv_namespaces = [
    { binding = "LEADERBOARD_KV", id = "KVSTORE_ID", preview_id = "KVSTORE_ID" }
]

[build]
command = "npm install && npm run webpack"
watch_dir = "cfworker"
[build.upload]
format = "service-worker"
